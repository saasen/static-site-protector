const express = require('express');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

require('dotenv').config();

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.APP_URL + '/callback'
}, (access_token, refresh_token, profile, cb) => {
    if (profile._json.domain === process.env.APP_ACCEPTED_DOMAIN) {
        return cb(null, profile);
    }

    return cb(null, false);
}));

passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

const app = express();

app.use(require('cookie-parser')());
app.use(require('express-session')({
    secret: process.env.APP_SECRET,
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.get('/login',
    passport.authenticate('google', {
        scope: ['profile', 'email']
    }));

app.get('/callback',
    passport.authenticate('google', {
        failureRedirect: '/login'
    }),
    function (req, res) {
        res.redirect('/');
    });

// Serves all remaining requests as static files
app.use(
    require('connect-ensure-login').ensureLoggedIn(),
    express.static(process.env.APP_FILE_LOCATION)(req, res, next));

app.listen(process.env.PORT || 3000);
